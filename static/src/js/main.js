function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  

odoo.define('xs.passwordmanager', function (require) {"use strict";

    var form_controller = require('web.FormController');
    var menu = require('web.AppsMenu');
    
    menu.include({
        _openApp: function(app) {
            this._super.apply(this, arguments);
            if(app.xmlID == 'xs_passwordmanager.menu_root') {

                var pwd = getCookie("xs_passwordmanager");

                if (pwd == "") {
                    var pwd = prompt("Please enter password:");
                }
                this._rpc({
                    model: 'res.users',
                    method: 'password_hash',
                    args: []
                }).then(function(res){

                    var first_hash = sjcl.hash.sha256.hash(pwd);
                    var first_hash_hex = sjcl.codec.hex.fromBits(first_hash);
    
                    if(res == first_hash_hex) {
                        setCookie("xs_passwordmanager", pwd, 30);
                    } else {
                        alert('Wrong Password!');
                        document.cookie = "";
                    }
                });
            }
        }
    });

    form_controller.include({
        saveRecord: function(event) {
            if(this.modelName == 'xs.passwordmanager'){

                var pwd =  getCookie("xs_passwordmanager");
                if (pwd != "") {
                    var new_value = sjcl.encrypt(pwd, document.getElementsByName('value')[0].value);
                    document.getElementsByName('value')[0].value = new_value;
                }
            }
            this._super(event);
        },
        _onButtonClicked: function(event) {
            if(event.data.attrs.custom == "xs_passwordmanager.decrypt"){
                //var obj_id = event.data.record.data.id
                var pwd =  getCookie("xs_passwordmanager");
                alert(pwd);
                if (pwd != "") {
                    var new_value = sjcl.decrypt(pwd, document.getElementsByName('value')[0].textContent);
                    document.getElementsByName('value')[0].textContent = new_value;
                }
            }
            this._super(event);
        },
    });
});