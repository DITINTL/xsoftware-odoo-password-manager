# -*- coding: utf-8 -*-

from odoo import models, fields, api
from hashlib import sha256

class xs_passwordmanager(models.Model):
    _name = 'xs.passwordmanager'
    _description = "Keep your passwords always safe with Password Manager"

    name = fields.Char(required=True, string="Friendly Name")
    value = fields.Text(required=True, string="Encrypted value")
    expire_date = fields.Date(string="Expire date to notify")

class xs_passwordmanager_users(models.Model):
    _inherit = 'res.users'

    xs_passwordmanager_hash = fields.Char(string = "Password Manager Hash")

    @api.onchange('xs_passwordmanager_hash')
    def password_hash(self):
        if self.xs_passwordmanager_hash
            self.xs_passwordmanager_hash = sha256(self.xs_passwordmanager_hash.encode('UTF-8')).hexdigest()